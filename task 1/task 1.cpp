﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
int main(){
	char str[100] = {0};
	int count = 0;
	printf("Enter a string: ");
	gets_s(str);
	for (int i = 0; i < strlen(str); i++)
		if (isalpha(str[i]) && islower(str[i])) count++;
	printf("There are %d lowercase latin letters\n", count);
    return 0;
}