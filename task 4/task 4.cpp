﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <Windows.h>

void PlayMorze(char* str) {
	for (int i = 0; i < strlen(str); i++) {
		if (str[i] == '*') 
			Beep(700, 300);
		else if (str[i] == '-') 
			Beep(700, 850);
	}
	Sleep(800);
}

int main() {
	char str[100] = { 0 }, morze[][36] = {
		"* -\n", 
		"- * * *\n", 
		"- * - *\n",
		"- * *\n",
		"*\n",
		"* * - *\n",
		"- - *\n",
		"* * * *\n",
		"* *\n",
		"* - - -\n",

		"- * -\n",
		"* - * *\n",
		"- -\n",
		"- *\n",
		"- - -\n",
		"* - - *\n",
		"- - * -\n",
		"* - *\n",
		"* * *\n",
		"-\n",

		"* * -\n",
		"* * * -\n",
		"* - -\n",
		"- * * -\n",
		"- * - -\n",
		"- - * *\n",

		"- - - - -\n",
		"* - - - -\n",
		"* * - - -\n",
		"* * * - -\n",
		"* * * * -\n",
		"* * * * *\n",
		"- * * * *\n",
		"- - * * *\n",
		"- - - * *\n",
		"- - - - *\n"
	};
	printf("Enter a string: ");
	gets_s(str);
	Sleep(300);
	for (int i = 0; i < strlen(str); i++){
		if (isalnum(str[i]) && isalpha(str[i])){
			printf("%s", morze[tolower(str[i]) - 97]); 
			PlayMorze(morze[tolower(str[i]) - 97]);
		}
		else if (isalnum(str[i])) {
			printf("%s", morze[str[i] - 48 + 26]);
			PlayMorze(morze[str[i] - 48 + 26]);
		}
	}	
}