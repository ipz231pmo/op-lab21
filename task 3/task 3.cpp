﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

int main() {
	system("chcp 1251"); system("cls");
	char str[100] = { 0 }, strRes[100] = {0}, longestWord[100] = {0};
	int countLeftBracket = 0, countRightBracket = 0, longestWordCount = 0;
	printf("Enter a string: ");
	gets_s(str);
	char* delim = strtok(str, " ");
	while (delim){
		int count = 0, check = 1;
		for (int i = 0; i < strlen(delim); i++) {
			count++;
			if (!isalpha(delim[i])) check = 0;
		}
		if (count > longestWordCount) { 
			strcpy(longestWord, delim);
			longestWordCount = count; 
		}
		if (!check) { 
			strcat(strRes, delim); 
			strcat(strRes, " ");
		}
		delim = strtok(NULL, " ");
	}
	if (countLeftBracket == countRightBracket)
		printf("Count of left and right brackets are equal\n");
	else
		printf("Count of left and right brackets are not equal\n");
	printf("Longest word is \"%s\"\n", longestWord);
	printf("String after deleting: %s\n", strRes);
	return 0;
}