﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
int main() {
	char str[100], strRes[100] = {0};
	printf("Enter a string: ");
	gets_s(str);
	char* delim = strtok(str, " ");
	while (delim) {
		strcat(strRes, delim);
		strcat(strRes, " ");
		delim = strtok(NULL, " ");
	}
	printf("%s", strRes);
	return 0;
}